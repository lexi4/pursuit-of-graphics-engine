﻿#pragma once
#include "Button.h"

class UParentA
{
public:
    virtual void Writeshit();
};

class UParentB
{
public:
    virtual void MakeShit();
};

class UColorfulButton : public UParentA, public IButton 
{
public:
    // ~Begin a Base Class Interface
    void Writeshit() override;
    // ~End a Base Class Interface
    
    // ~Begin IButton Interface
    void Click() override;
    void Hide() override;
    void Show() override;
    void Kill() override;
    // ~End IButton Interface
};

class UShitButton : public UParentB, public IButton
{
public:
    // ~Begin b Base Class Interface
    void MakeShit() override;
    // ~End b Base Class Interface

    // ~Begin IButton Interface
    void Click() override;
    void Hide() override;
    void Show() override;
    void Kill() override;
    // ~End IButton Interface
};
