﻿#include "DelegateTest.h"

#include "Button.h"
#include "ColorfulButton.h"

#include "Core/CoreTypes.h"

void UDelegateTest::Execute()
{
    InitButton(ColorfulButton);
    InitButton(ShitButton);
    InitButton(AbcButton);

    ColorfulButton->Writeshit();
    ShitButton->MakeShit();

    std::vector Buttons {
        Cast<IButton>(ColorfulButton),
        Cast<IButton>(ShitButton),
        Cast<IButton>(AbcButton)
    };

    TArray<IButton*> Buttons1 {
            Cast<IButton>(ColorfulButton),
            Cast<IButton>(ShitButton),
            Cast<IButton>(AbcButton),
    };

    for(IButton* Button : Buttons)
    {
        Button->Click();
        Button->Hide();
        Button->Show();
        Button->Kill();
    }
}

void UDelegateTest::OnClickHandler()
{
    std::cout << "void OnClickHandler()" << std::endl;
}

void UDelegateTest::OnCallHandler(float b, int c)
{
    std::cout << "void OnCallHandler : " << b << " , " << c << std::endl;
}

bool UDelegateTest::CheckAny(const EMyEnum& InEnum, const TArray<EMyEnum>& InValues)
{
    for(auto InValue : InValues)
    {
        if(InEnum == InValue)
        {
            return true;
        }
    }
    return false;
}

template <class T>
void UDelegateTest::InitButton(T*& ClassInstancePointer)
{
    InitObject(ClassInstancePointer);
    if(IButton* Button = Cast<IButton>(ClassInstancePointer))
    {
        Button->OnClick += DelegateLib::MakeDelegate(this, &UDelegateTest::OnClickHandler);
    }
}
