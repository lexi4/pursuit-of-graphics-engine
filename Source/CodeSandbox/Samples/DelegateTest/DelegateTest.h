#pragma once

#include "CodeSandbox/Samples/SandboxInterface.h"
#include "Core/CoreTypes.h"

class IButton;
class UColorfulButton;
class UShitButton;

enum class EMyEnum
{
    Default = 0,
    First = 1,
    Second = 2,
};

class UDelegateTest : public ISandbox
{
public:
    void Execute() override;

protected:
    template<class T>
    void InitButton(T*& ClassInstancePointer);

    void OnClickHandler();
    void OnCallHandler(float b, int c);
    bool CheckAny(const EMyEnum& InEnum, const TArray<EMyEnum>& InValues);

protected:
    IButton* MyButton = nullptr;

    UColorfulButton* ColorfulButton = nullptr;
    UShitButton* ShitButton = nullptr;
    UShitButton* AbcButton = nullptr;
};
