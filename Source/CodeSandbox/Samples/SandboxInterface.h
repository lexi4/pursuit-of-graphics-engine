﻿#pragma once
#include "Core/CoreTypes.h"

class ISandbox
{
public:
    virtual ~ISandbox() = default;
    virtual void Execute() = 0;

public:

    template<class T>
    void SetSandboxTestName(T* InTestCase)
    {
        TypeName = TypeName::TypeName<decltype(InTestCase)>();
    }
    FTypeName TypeName;
};