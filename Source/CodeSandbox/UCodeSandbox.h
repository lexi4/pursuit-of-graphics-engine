﻿#pragma once
#include "Samples/SandboxInterface.h"
#include "Core/CoreTypes.h"

class UCodeSandbox
{
public:
    void Execute();

protected:
    template<class T>
    ISandbox* PrepareTest(T*& InTest)
    {
        InitObject(InTest);

        ISandbox* TestCase = Cast<ISandbox>(InTest);
        TestCase->SetSandboxTestName(InTest);
        return TestCase;
    }
};
