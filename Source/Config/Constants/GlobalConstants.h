﻿#pragma once

#include "Core/CoreTypes.h"

static constexpr int32 UNDEFINED_VALUE = -1;
static const FString UNDEFINED_STRING = "UNDEFINED_STRING";
static constexpr float UNDEFINED_VALUE_FLOAT = -1.0f;
static constexpr double UNDEFINED_VALUE_DOUBLE = -1.0f;
