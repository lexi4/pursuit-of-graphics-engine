﻿#pragma once

#include "Core/CoreTypes.h"
#include "Render/Data/ERenderAPI.h"
#include "Render/Window/Window.h"

namespace CONST_RENDER
{
    inline const ERenderAPI RENDER_API = ERenderAPI::Vulkan;

    namespace WINDOW
    {
        inline const FVector2D DEFAULT_SIZE = {800, 600};
        inline const FString NAME = "PursuitEngine";
    }

    struct FVulkanLayersAndExtensions;

    namespace VULKAN
    {
        inline constexpr bool IS_ENABLED_VALIDATION_LAYERS = true;
        inline const FString VK_VALIDATION_LAYER_NAME = "VK_LAYER_KHRONOS_validation";
        inline const FString VK_API_DUMP_VALIDATION_LAYER_NAME = "VK_LAYER_LUNARG_api_dump";

        inline const FString VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME = {
            #ifdef PLATFORM_WINDOWS
            "VK_KHR_win32_surface"
            #endif
        };

        struct FVulkanLayersAndExtensions
        {
        public:
            void Init()
            {
                 InstanceExtensions.Emplace("VK_KHR_surface");

                 //Platform specified Surface extensions (for GLFW)
                 InstanceExtensions.Emplace(VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME);
                
                 //Validation Layers
                 if (IS_ENABLED_VALIDATION_LAYERS)
                 {
                     InstanceExtensions.Emplace(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
                     Layers.Emplace(VK_VALIDATION_LAYER_NAME);
                     Layers.Emplace(VK_API_DUMP_VALIDATION_LAYER_NAME);
                 }
                
                 //Swap Chain
                 DeviceExtensions.Emplace(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
            }

            const TArray<FString>& GetInstanceExtensions() const { return InstanceExtensions; }
            const TArray<FString>& GetDeviceExtensions() const { return DeviceExtensions; }
            const TArray<FString>& GetLayers() const { return Layers; }

        protected:
            TArray<FString> DeviceExtensions;
            TArray<FString> InstanceExtensions;
            TArray<FString> Layers;
        };

        //Should be initialized after GLFW and before Vulkan startup.
        inline FVulkanLayersAndExtensions LAYERS_AND_EXTENSIONS;
    }
}
