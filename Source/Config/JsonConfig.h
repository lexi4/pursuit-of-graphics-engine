﻿#pragma once

#include <json.hpp>

#include "Core/BaseStructure.h"

#include "Constants/RenderConstants.h"
#include "Core/FileSystem.h"

using FJson = nlohmann::json;

#define CONFIG(Name) Config[STRINGIFY(Name)]

struct FJsonConfig : FBaseStructure
{
public:
    FVector2D ScreenResolution = CONST_RENDER::WINDOW::DEFAULT_SIZE;
    EGraphicsPresentationMode PresentationMode = EGraphicsPresentationMode::TripleBuffer;
    bool GetIsLoaded() const { return IsLoaded; }

public:
    void Load()
    {
        const bool IsFileExist = exists(FileSystem::CONFIG_FILE_PATH);
        if (!IsFileExist)
        {
            create_directories(FileSystem::CONFIG_FOLDER_PATH);
            Reset();
        }

        //Fillup Json object
        FReadFile JsonConfigFile(FileSystem::CONFIG_FILE_PATH);
        JsonConfigFile >> Config;

        const auto& Value = CONFIG(ScreenResolution);
        ScreenResolution = {Value[0].get<float>(),Value[1].get<float>()};
        PresentationMode = static_cast<EGraphicsPresentationMode>(CONFIG(PresentationMode).get<uint8>());

        IsLoaded = true;
    }

    void Save()
    {
        CONFIG(ScreenResolution) = FJson::array({ScreenResolution.X, ScreenResolution.Y});

        //0 - No VSync, 1 - VSync, 2 - Adaptive VSync, 3 - Triple Buffer VSync
        CONFIG(PresentationMode) = PresentationMode;

        //Save Json to file
        FWriteFile JsonConfigFile(FileSystem::CONFIG_FILE_PATH);
        JsonConfigFile << std::setw(4) << Config << std::endl;
        JsonConfigFile.close();
    }

    void Reset()
    {
        *this = FJsonConfig();
        Save();
    }

protected:
    FJson Config = FJson();
    bool IsLoaded = false;
};

inline FJsonConfig* JsonConfig = new FJsonConfig();

#undef CONFIG