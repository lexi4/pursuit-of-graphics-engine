﻿#pragma once

class UBaseClass
{
public:
    UBaseClass() = default;
    virtual ~UBaseClass() = default;

public:
    virtual bool IsValid()
    {
        return true;
    }

    IS_VALID_IMPL()
};