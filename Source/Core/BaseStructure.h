﻿#pragma once

struct FBaseStructure
{
public:
    FBaseStructure() = default;
    virtual ~FBaseStructure() = default;

public:
    virtual bool IsValid()
    {
        return true;
    }
};