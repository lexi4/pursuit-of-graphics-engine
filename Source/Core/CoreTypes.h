﻿#pragma once

//Platform defines
#if defined(_WIN32) || defined(_WIN64)
    #define _WIN32_WINDOWS 0x0601
    #define _WINSOCK_DEPRECATED_NO_WARNINGS
    #define PLATFORM_WINDOWS
#endif

#define VULKAN_HPP_NO_NODISCARD_WARNINGS
#include <vulkan/vulkan.hpp>

#include <MulticastDelegate.h>
#include <asio.hpp>

//STL
#include <string>
#include <iostream>
#include <filesystem>
#include <type_traits>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include "Time.h"
#include "StandardLibrary/Array.h"
#include "StandardLibrary/TypeAliases.h"
#include "Vectors/Vector2D.h"
#include "Vectors/Vector3D.h"

//Types
// template<class T>
// using TArray = std::vector<T>;

#define ensureMsgf(exp, msg) assert(((void)msg, (exp)))
#define ensure(exp) assert(!(exp))

inline TArray<FString>* CharsToStrings(const char** Chars)
{
    const auto Result = new TArray<FString>(Chars, Chars + (sizeof(Chars) / sizeof(*Chars)));
    return Result;
}

static void StringsToCharsVector(const TArray<FString>& Strings, std::vector<char const*>& Result)
{
    for (auto const& String : Strings)
    {
        Result.push_back(String.data());
    }
}

static void StringsToCharsTArray(const TArray<FString>& Strings, TArray<char const*>& Result)
{
    for (auto const& String : Strings)
    {
        Result.Emplace(String.data());
    }
}

template<class T,class T2>
T* Cast(T2*& ClassInstancePointer)
{
    return dynamic_cast<T*>(ClassInstancePointer);
}

template<class T,class T2>
T* StaticCast(T2*& ClassInstancePointer)
{
    return static_cast<T*>(ClassInstancePointer);
}

template<class T>
void InitObject(T*& InValue)
{
    if(InValue == nullptr)
    {
        InValue = new T();
    }
}

enum class EGraphicsPresentationMode : uint8
{
    None = 0,
    VSync = 1,
    Adaptive = 2,
    TripleBuffer = 3
};

namespace TypeName {
    using namespace std;
    template <typename T>
     constexpr string_view wrapped_type_name () {
#ifdef __clang__
        return __PRETTY_FUNCTION__;
#elif defined(__GNUC__)
        return  __PRETTY_FUNCTION__;
#elif defined(_MSC_VER)
        return  __FUNCSIG__;
#endif
    }

    class probe_type;
    constexpr string_view probe_type_name ("typeName::probe_type");
    constexpr string_view probe_type_name_elaborated ("class typeName::probe_type");
    constexpr string_view probe_type_name_used (wrapped_type_name<probe_type> ().find (probe_type_name_elaborated) != -1 ? probe_type_name_elaborated : probe_type_name);

    constexpr size_t prefix_size () {
        return wrapped_type_name<probe_type> ().find (probe_type_name_used);
    }

    constexpr size_t suffix_size () {
        return wrapped_type_name<probe_type> ().length () - prefix_size () - probe_type_name_used.length ();
    }

    template <typename T>
    string_view TypeName () {
        constexpr auto type_name = wrapped_type_name<T> ();

        return type_name.substr (prefix_size (), type_name.length () - prefix_size () - suffix_size ());
    }
}

#pragma warning(disable : 4003)
#define LOG(__VA_ARGS__) \
std::cout << Time::GetTimeSinceStartUp() << " [" << __FUNCTION__ << "] " << FString({__VA_ARGS__}) << std::endl \

#define PRINT_FUNC() (LOG(""))

#define ENUM_HAS_ANY_FLAGS(Enum, Flags) (Enum & Flags) == Flags

template<class T>
FString ToString(T Value)
{
    return std::to_string(Value);
}

#define IS_VALID_IMPL() \
template<class T> \
bool IsValid(T Value) \
{ \
    return Value != nullptr; \
} 

IS_VALID_IMPL()

#define STRINGIFY(name) (#name)

namespace TArrayUtils
{
    template<class T> 
    bool Contains(const TArray<T>& InArray, const T& TargetElement)
    {
        for(const T& Elem : InArray)
            if(Elem == TargetElement)
                return true;

        return false;
    }

    template<class T> 
    T* FindByPredicate(const TArray<T>& InArray, const std::function<bool(const T&)>& Predicate)
    {
        static_assert(Predicate, "Invalid Predicate");

        for(const T& Elem : InArray)
            if(Predicate(Elem))
                return Elem;

        return nullptr;
    }
}