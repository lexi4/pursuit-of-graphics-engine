﻿#pragma once 
#include "CoreTypes.h"

#include <fstream>

//FileSystem
namespace fs = std::filesystem;
typedef fs::path FPath;
typedef std::ofstream FWriteFile;
typedef std::ifstream FReadFile;

namespace FileSystem
{
    static const FPath PROJECT_ROOT_PATH = fs::current_path();
    static const FPath CONFIG_FOLDER_PATH = PROJECT_ROOT_PATH / "Config";

    const static FPath CONFIG_FILE_PATH = CONFIG_FOLDER_PATH / "Config.json";
}
