﻿#pragma once

#include "Common.h"
#include "StringUtils.h"

template <typename BaseT>
struct TArray
{
    /*
    *  MACRO DEFINE
    */
    #define TypeCheck(ArgT) static_assert(IsSame<BaseT,ArgT>());
    #define ArrIndex (Size - 1)
    #define AllocationThreshold 10

    using Iterator = ::Iterator<BaseT>;
    /*
    *  METHODS
    */

    FORCEINLINE const BaseT*& GetData() const
    {
        return Data;
    }

    FORCEINLINE BaseT*& GetDataMutable() const
    {
        return Data;
    }

    FORCEINLINE void Empty(const size_t NewSize = 0)
    {
        Size = NewSize;
        delete[] Data;
        Data = new BaseT[NewSize];
    }

    FORCEINLINE size_t Num() const
    {
        return Size;
    }

    FORCEINLINE bool IsEmpty() const
    {
        return Size < 1;
    }

    FORCEINLINE void Emplace(BaseT NewElem)
    {
        const size_t NewSize = Size + 1;
        Resize(NewSize);
        Data[NewSize - 1] = MoveTemp(NewElem);
    }

    FORCEINLINE void Emplace(const TArray<BaseT>& Other)
    {
        const size_t OldSize = Size;
        const size_t NewSize = OldSize + Other.Size;

        Resize(NewSize);

        for (size_t Idx = OldSize - 1; Idx < NewSize; ++Idx)
        {
            Data[Idx] = Other.Data[Idx];
        }
    }

    FORCEINLINE void Resize(const size_t NewSize)
    {
        if (NewSize == Size)
        {
            return;
        }

        const bool IsNotEnough = Capacity < NewSize;
        const size_t DeltaCapacity = Abs((intptr_t)Capacity - (intptr_t)NewSize);
        const bool IsReachThreshold = DeltaCapacity > AllocationThreshold;

        //Reallocate and fill Data
        if (IsNotEnough || IsReachThreshold)
        {
            Capacity = AllocationThreshold * static_cast<size_t>(ceil((float)NewSize / (float)AllocationThreshold));
            BaseT* NewData = new BaseT[Capacity];
            for (size_t Idx = 0; Idx < Min(NewSize, Size); ++Idx)
            {
                NewData[Idx] = MoveTemp(Data[Idx]);
            }
            delete[] Data;
            Data = NewData;
        }
        Size = NewSize;
    }

    FORCEINLINE bool IsValidIndex(const size_t Idx) const
    {
        return InRange(Idx, 0, Size - 1);
    }

    FORCEINLINE bool Contains(const BaseT& TargetElement)
    {
        for (const BaseT& Elem : this)
            if (Elem == TargetElement)
                return true;

        return false;
    }

    FORCEINLINE BaseT* FindByPredicate(const std::function<bool(const BaseT&)>& Predicate)
    {
        static_assert(Predicate, "Invalid Predicate");

        for (const BaseT& Elem : this)
            if (Predicate(Elem))
                return Elem;

        return nullptr;
    }

private:
    [[nodiscard]] std::string ArrayElemToString(BaseT Elem) const
    {
        std::string Out = StringUtils::ToString<BaseT>(std::forward<BaseT>(Elem));
        StringUtils::AddQuotes(Out);
        StringUtils::AddPadding(Out, 4);
        return Out;
    }

public:
    FORCEINLINE std::string ToString() const
    {
        std::string Out;
        Out += "Capacity: " + std::to_string(Capacity) + ", Size: " + std::to_string(Size) + " : {\n";
        size_t Idx = 0;
        for (BaseT& Elem : *this)
        {
            Out += ArrayElemToString(Elem);
            if (++Idx < Size)
                Out += ",\n";
        }
        Out += "\n}";
        return Out;
    }

    FORCEINLINE std::string ToStringWithCapacity() const
    {
        std::string Out;
        Out += "Capacity: " + std::to_string(Capacity) + ", Size: " + std::to_string(Size) + " : {\n";
        for (BaseT& Elem : *this)
            Out += ArrayElemToString(Elem) + ",\n";

        const size_t DeltaCapacity = Capacity - Size;
        for (size_t Idx = 0; Idx < DeltaCapacity; ++Idx)
        {
            Out += "#EMPTY#";
            if (Idx < DeltaCapacity - 1)
                Out += ",\n";
        }
        Out += "\n}";
        return Out;
    }

    /*
    *  Constructors
    */

    TArray() = default;

    TArray(const TInitializerList<BaseT>& InInitializerList)
    {
        TArray(InInitializerList.begin(), InInitializerList.end());
    }

    TArray(const std::vector<BaseT>& InVector)
    {
        TArray(InVector.begin(), InVector.end());
    }

    TArray(std::vector<BaseT>&& InVector)
    {
        auto InFirst = InVector.begin();
        auto InLast = InVector.end();
        for (; InFirst != InLast; ++InFirst)
        {
            Emplace(MoveTemp(*InFirst));
        }
    }

    TArray(TArray<BaseT>&& InVector) noexcept
    {
        auto InFirst = InVector.begin();
        auto InLast = InVector.end();
        for (; InFirst != InLast; ++InFirst)
        {
            Emplace(MoveTemp(*InFirst));
        }
    }

    template <class T>
    TArray(T InFirst, T InLast)
    {
        for (; InFirst != InLast; ++InFirst)
        {
            Emplace(*InFirst);
        }
    }

    [[nodiscard]] std::vector<BaseT> ToVector() const
    {
        return std::vector<BaseT>(Data, Data + Size);
    }

    /*
    *  Operators
    */

    FORCEINLINE TArray<BaseT>& operator=(TArray<BaseT>&& rhs) noexcept
    {
        Data = Exchange(rhs.Data, nullptr);
        Size = Exchange(rhs.Size, 0);
        Capacity = Exchange(rhs.Capacity, 0);
        return *this;
    }

    FORCEINLINE BaseT& operator[](const size_t InIndex)
    {
        return Data[InIndex];
    }

    FORCEINLINE const BaseT& operator[](const size_t InIndex) const
    {
        return Data[InIndex];
    }

    /*
    *  MACRO UNDEFINE
    */
    #undef TypeCheck
    #undef ArrIndex
    #undef AllocationThreshold

    //Foreach implementation
    Iterator begin() { return Iterator(Data); }
    Iterator begin() const { return Iterator(Data); }
    Iterator end() { return Iterator(Data + Size); }
    Iterator end() const { return Iterator(Data + Size); }

protected:
    size_t Size = 0;
    size_t Capacity = 0;
    BaseT* Data = nullptr;
};
