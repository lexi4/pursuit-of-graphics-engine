﻿#pragma once

template <class T, T TValue>
struct TIntegralConstant
{
    static constexpr T Value = TValue;

    using ValueType = T;
    using Type = TIntegralConstant;

    constexpr operator ValueType() const noexcept { return Value; }
    constexpr ValueType operator()() const noexcept { return Value; }
};

template <bool T>
using TBoolConstant = TIntegralConstant<bool, T>;

template <class, class>
inline constexpr bool TIsSameInternal = false;
template <class T>
inline constexpr bool TIsSameInternal<T, T> = true;

template <class TOne, class TTwo>
using TIsSame = TBoolConstant<TIsSameInternal<TOne, TTwo>>;

template <class T, class Other = T>
T Exchange(T& Value, Other&& NewValue) noexcept
{
    T OldValue = static_cast<T&&>(Value);
    Value = static_cast<Other&&>(NewValue);
    return OldValue;
}

//Remove pointer

template <class T>
struct RemovePointerInternal
{
    using Type = T;
};

template <class T>
struct RemovePointerInternal<T*>
{
    using Type = T;
};

template <class T>
struct RemovePointerInternal<T* const>
{
    using Type = T;
};

template <class T>
struct RemovePointerInternal<T* volatile>
{
    using Type = T;
};

template <class T>
struct RemovePointerInternal<T* const volatile>
{
    using Type = T;
};

template <class T>
using TRemovePointer = typename RemovePointerInternal<T>::Type;

//Remove reference

template <class T>
struct RemoveReferenceInternal
{
    using Type = T;
};

template <class T>
struct RemoveReferenceInternal<T&>
{
    using Type = T;
};

template <class T>
struct RemoveReferenceInternal<T&&>
{
    using Type = T;
};

template <class T>
using TRemoveReference = typename RemoveReferenceInternal<T>::Type;

//Remove Constant Volatile

template <class T>
struct RemoveCVInternal
{
    using Type = T;
};

template <class T>
struct RemoveCVInternal<T const>
{
    using Type = T;
};

template <class T>
struct RemoveCVInternal<T volatile>
{
    using Type = T;
};

template <class T>
struct RemoveCVInternal<T const volatile>
{
    using Type = T;
};

template <class T>
using TRemoveCV = typename RemoveCVInternal<T>::Type;

// Move

template <class T>
[[nodiscard]] constexpr TRemoveReference<T>&& MoveTemp(T&& Argument) noexcept
{
    // forward Argument as movable
    return static_cast<TRemoveReference<T>&&>(Argument);
}

template <class T>
using PureType = TRemoveCV<TRemovePointer<TRemoveReference<T>>>;

template <class T>
constexpr size_t PureTypeSize()
{
    return sizeof(PureType<T>);
}

constexpr size_t PureTypeSize(auto Value)
{
    return PureTypeSize<decltype(Value)>();
}

template <class TOne, class TTwo, class TThree>
FORCEINLINE bool InRange(const TOne Value, const TTwo Min, const TThree Max)
{
    return (Value >= static_cast<TOne>(Min)) && (Value <= static_cast<TOne>(Max));
}

template <class TOne, class TTwo>
FORCEINLINE TOne Min(const TOne One, const TTwo Other)
{
    return One > static_cast<TOne>(Other) ? static_cast<TOne>(Other) : One;
}


// Absolute

FORCEINLINE float Abs (float Value)
{
    return Value < 0 ? -Value : Value;
}

FORCEINLINE double Abs (double Value)
{
    return Value < 0 ? -Value : Value;
}

FORCEINLINE size_t Abs (intptr_t Value)
{
    return Value < 0 ? -Value : Value;
}

// Initializer List

/*
 * Compilers didn't allow use custom initializer lists. That's crazy.
 * That's as syntax sugar similar to ForEach implementation.
 * Only std::initializer_list for now..
 */
template <class T>
using TInitializerList = std::initializer_list<T>;

// Iterators

template <class T>
struct Iterator
{
    T* Ptr;
    Iterator() = delete;

    Iterator(T* InPtr) : Ptr(InPtr)
    {
    }

    bool operator!=(Iterator rhs) { return Ptr != rhs.Ptr; }
    T& operator*() { return *Ptr; }
    void operator++() { ++Ptr; }
};

// IsPointer

template <class>
inline constexpr bool IsPointerInternal = false;

template <class T>
inline constexpr bool IsPointerInternal<T*> = true;

template <class T>
inline constexpr bool IsPointerInternal<T* const> = true;

template <class T>
inline constexpr bool IsPointerInternal<T* volatile> = true;

template <class T>
inline constexpr bool IsPointerInternal<T* const volatile> = true;

template <class T>
using TIsPointer = TBoolConstant<IsPointerInternal<T>>;

// Template: EnableIf
/* Renamed Copypaste from xtr1common */

template <bool Test, class T = void>
struct EnableIfInternal
{
    // no member "type" when !_Test
}; 

template <class T>
struct EnableIfInternal<true, T>
{
    // type is _Ty for _Test
    using Type = T;
};

template <bool Test, class T = void>
using TEnableIf = typename EnableIfInternal<Test, T>::Type;
