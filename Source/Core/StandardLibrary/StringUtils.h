﻿#pragma once

#include "TypeAliases.h"

namespace StringUtils
{
    inline FString& AddQuotes(FString& Text)
    {
        Text = "\"" + Text + "\"";
        return Text;
    }

    inline std::string& AddPadding(FString& Text,int32 Padding)
    {
        std::string PaddingChars;
        for(int32 Idx = 0; Idx < Padding; ++Idx)
            PaddingChars += " ";
        Text = PaddingChars + Text;
        return Text;
    }

    template<typename T> requires std::is_arithmetic_v<T>
    std::string ToString(const T Elem) { return std::to_string(Elem); }
    
    template<typename T> requires std::is_convertible_v<T, std::string>
    std::string ToString(const T& Elem) { return Elem; }
    
    template<typename T>
    std::string ToString(const T& Elem) { return std::string("#VALUE#"); }
}
