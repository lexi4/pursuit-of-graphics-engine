﻿#pragma once

#include <memory>
#include <optional>
#include <set>
#include <string>
#include <unordered_map>

//Literals
typedef std::string_view FTypeName;
typedef std::string FString;
typedef char* FCharString;

//Math
typedef uint32_t uint32;
typedef int32_t int32;
typedef uint8_t uint8;
typedef int8_t int8;
typedef uint16_t uint16;
typedef int16_t int16;
typedef uint64_t uint64;
typedef int64_t int64;

template<class T>
using TOptional = std::optional<T>;

template<class T>
using TSharedPtr = std::shared_ptr<T>;

template<class T>
using TWeakPtr = std::weak_ptr<T>;

template<class TOne, class TTwo>
using TMap = std::unordered_map<TOne, TTwo>;

template<class T>
using TSet = std::set<T>;