#pragma once

#include <chrono>

namespace Time {
	static const auto StartUpTime = std::chrono::high_resolution_clock::now();
	static auto CurrentTime = std::chrono::high_resolution_clock::now();
	static std::chrono::duration<double> TimeSinceStartUp;

	static void UpdateTime() {
		CurrentTime = std::chrono::high_resolution_clock::now();
		TimeSinceStartUp = CurrentTime - StartUpTime;
	}
	static double GetTimeSinceStartUp() {
		UpdateTime();
		return TimeSinceStartUp.count();
	}
}

