﻿#include "Vector2D.h"

FVector2D::FVector2D()
{
    X = 0.0f;
    Y = 0.0f;
}

FVector2D::FVector2D(float InX,float InY)
    :	X(InX), Y(InY)
{
}

FVector2D::FVector2D(float InAll)
{
    FVector2D(InAll,InAll);
}

FVector2D::FVector2D(const FVector2D& InValue) : X(InValue.X), Y(InValue.Y) 
{
}

bool FVector2D::operator==(const FVector2D& InSecond) const
{
    return this->X == InSecond.X; 
}

bool FVector2D::operator!=(const FVector2D& InSecond) const
{
    return !(*this == InSecond);
}