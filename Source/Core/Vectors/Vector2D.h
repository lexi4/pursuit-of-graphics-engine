﻿#pragma once
#include "Core/CoreTypes.h"

struct FVector2D
{
public:
    FVector2D();
    FVector2D(float InX, float InY);
    FVector2D(float InAll);
    FVector2D(const FVector2D& InValue);

    float X = 0.0f;
    float Y = 0.0f;

public:
    bool operator==(const FVector2D& InSecond) const;
    bool operator!=(const FVector2D& InSecond) const;
};
