﻿#pragma once

enum class ERenderAPI : uint8
{
    Undefined = 0,
    Vulkan = 1<<0,
    Dx12 = 1<<1,
};