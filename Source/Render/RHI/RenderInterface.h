﻿#pragma once

class UWindow;

class IRender
{
public:
    virtual ~IRender() = default;
    virtual void RenderFrame() = 0;
};
