﻿#include "VulkanDevice.h"

UVulkanDevice::UVulkanDevice(vk::PhysicalDevice InPhysicalDevice, const TArray<FString>& InDeviceExtensions, const TArray<FString>& InLayers)
{
    if (!InPhysicalDevice)
    {
        LOG("INVALID DEVICE!");
        exit(1);
    }
    PhysicalDevice = InPhysicalDevice;

    //Convert Extensions and Layer names.
    std::vector<const char*> DeviceExtensions, Layers;
    StringsToCharsVector(InDeviceExtensions, DeviceExtensions);
    StringsToCharsVector(InLayers, Layers);

    const TArray<vk::QueueFamilyProperties> QueueFamilyProperties = PhysicalDevice.getQueueFamilyProperties();

    //Make Queues creation data (and allocate priorities)
    const auto QueuesCreateInfo = Queues.MakeQueuesCreateInfo(QueueFamilyProperties);

    std::vector<vk::DeviceQueueCreateInfo> TempQueuesCreateInfo = QueuesCreateInfo.ToVector();
    vk::DeviceCreateInfo DeviceCreateInfo = vk::DeviceCreateInfo(vk::DeviceCreateFlags(), TempQueuesCreateInfo);
    DeviceCreateInfo.setPEnabledExtensionNames(DeviceExtensions);
    DeviceCreateInfo.setPEnabledLayerNames(Layers);
    LogicalDevice = PhysicalDevice.createDevice(DeviceCreateInfo);

    //Clear previously allocated Queues creation data (queue priorities)
    Queues.ClearQueuesCreateInfo();

    Queues.FillQueuePool(&LogicalDevice, QueueFamilyProperties);
}

bool UVulkanDevice::IsValid()
{
    return PhysicalDevice &&
            LogicalDevice &&
            Queues.IsValid();
}
