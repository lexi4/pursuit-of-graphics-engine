#pragma once

#include <Core/CoreTypes.h>

#include "VulkanQueuePool.h"

#include "Core/BaseClass.h"

class UVulkanDevice : public UBaseClass
{
public:
    UVulkanDevice() = delete;

    UVulkanDevice(vk::PhysicalDevice InPhysicalDevice, const TArray<FString>& InDeviceExtensions, const TArray<FString>& InLayers);
    ~UVulkanDevice() = default;

public:
    bool IsValid() override;

public:
    vk::PhysicalDevice* GetPhysicalDevice() { return &PhysicalDevice; }
    vk::Device* GetLogicalDevice() { return &LogicalDevice; }
    FVulkanQueuePool* GetQueues() { return &Queues; }

protected:
    vk::PhysicalDevice PhysicalDevice;
    vk::Device LogicalDevice;
    FVulkanQueuePool Queues = FVulkanQueuePool();
};
