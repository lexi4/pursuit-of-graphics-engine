﻿#pragma once

#include "Config/Constants/GlobalConstants.h"

struct FVulkanQueue
{
    vk::Queue Queue = nullptr;
    vk::QueueFlags QueueFamilyFlags;
    int32 FamilyIndex = UNDEFINED_VALUE;
    int32 QueueIndex = UNDEFINED_VALUE;

    FVulkanQueue() = default;
};
