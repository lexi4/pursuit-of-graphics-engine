﻿#include "VulkanQueuePool.h"
#include "VulkanQueue.h"

FVulkanQueuePool::~FVulkanQueuePool()
{
    ClearQueuePool();
    ClearQueuesCreateInfo();
}

TArray<vk::DeviceQueueCreateInfo> FVulkanQueuePool::MakeQueuesCreateInfo(
    const TArray<vk::QueueFamilyProperties>& QueueFamilyProperties)
{
    AllQueueFamilyPriorities.Empty();
    TArray<vk::DeviceQueueCreateInfo> QueuesCreateInfo;

    for (uint32 QueueFamilyIndex = 0; QueueFamilyIndex < QueueFamilyProperties.Num(); ++QueueFamilyIndex)
    {
        const auto& QueueFamilyProperty = QueueFamilyProperties[QueueFamilyIndex];
        std::vector<float>* QueuePriorities = new std::vector<float>();

        for (uint32 QueueIndex = 0; QueueIndex < QueueFamilyProperty.queueCount; ++QueueIndex)
            QueuePriorities->push_back(0.0f);

        vk::DeviceQueueCreateInfo QueueCreateInfo = vk::DeviceQueueCreateInfo(vk::DeviceQueueCreateFlags(), QueueFamilyIndex, *QueuePriorities);
        QueuesCreateInfo.Emplace(QueueCreateInfo);
        AllQueueFamilyPriorities.Emplace(QueuePriorities);
    }

    //All available queues.
    return QueuesCreateInfo;
}

void FVulkanQueuePool::ClearQueuesCreateInfo()
{
    //Need to remove initial allocations
    for (const auto& QueueFamilyPriority : AllQueueFamilyPriorities)
        delete QueueFamilyPriority;

    AllQueueFamilyPriorities.Empty();
}

void FVulkanQueuePool::FillQueuePool(vk::Device* Device, const TArray<vk::QueueFamilyProperties>& QueueFamilyProperties)
{
    ClearQueuePool();

    for (int32 QueueFamilyIndex = 0; QueueFamilyIndex < QueueFamilyProperties.Num(); ++QueueFamilyIndex)
    {
        const auto& QueueFamilyProperty = QueueFamilyProperties[QueueFamilyIndex];

        const bool IsGraphicsQueue = ENUM_HAS_ANY_FLAGS(QueueFamilyProperty.queueFlags, vk::QueueFlagBits::eGraphics);
        const bool IsComputeQueue = ENUM_HAS_ANY_FLAGS(QueueFamilyProperty.queueFlags, vk::QueueFlagBits::eCompute);
        const bool IsTransferQueue = ENUM_HAS_ANY_FLAGS(QueueFamilyProperty.queueFlags, vk::QueueFlagBits::eTransfer);
        const bool IsSparseQueue = ENUM_HAS_ANY_FLAGS(QueueFamilyProperty.queueFlags, vk::QueueFlagBits::eSparseBinding);

        for (int32 QueueIndex = 0; QueueIndex < (int32)QueueFamilyProperty.queueCount; ++QueueIndex)
        {
            FVulkanQueue* VulkanQueueInfo = new FVulkanQueue();
            VulkanQueueInfo->Queue = Device->getQueue(QueueFamilyIndex, QueueIndex);
            VulkanQueueInfo->FamilyIndex = QueueFamilyIndex;
            VulkanQueueInfo->QueueIndex = QueueIndex;
            VulkanQueueInfo->QueueFamilyFlags = QueueFamilyProperty.queueFlags;

            AllQueues.Emplace(VulkanQueueInfo);

            if (IsGraphicsQueue)
                GraphicsQueues.Emplace(VulkanQueueInfo);
            if (IsComputeQueue)
                ComputeQueues.Emplace(VulkanQueueInfo);
            if (IsTransferQueue)
                TransferQueues.Emplace(VulkanQueueInfo);
            if (IsSparseQueue)
                SparseQueues.Emplace(VulkanQueueInfo);
        }
    }
    LOG(
        "\n    Queues Count: " + ToString(SparseQueues.Num()) +
        "\n      * Graphics: " + ToString(GraphicsQueues.Num()) +
        "\n      * Computes: " + ToString(ComputeQueues.Num()) +
        "\n      * Transfer: " + ToString(TransferQueues.Num()) +
        "\n      * Sparse: " + ToString(SparseQueues.Num())
    );
}

void FVulkanQueuePool::ClearQueuePool()
{
    PRINT_FUNC();
    GraphicsQueues.Empty();
    ComputeQueues.Empty();
    TransferQueues.Empty();
    SparseQueues.Empty();

    for (const auto& VulkanQueueInfo : AllQueues)
        delete VulkanQueueInfo;

    AllQueues.Empty();
}

bool FVulkanQueuePool::IsValid()
{
    return AllQueues.Num() != 0;
}