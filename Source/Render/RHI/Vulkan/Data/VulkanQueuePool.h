#pragma once

#include <Render/RHI/Vulkan/VulkanIncludes.h>

#include "Core/BaseStructure.h"

struct FVulkanQueue;

struct FVulkanQueuePool : FBaseStructure
{
public:
    FVulkanQueuePool() = default;
    ~FVulkanQueuePool() override;

    // bool IsValid() override;

public:
    TArray<vk::DeviceQueueCreateInfo> MakeQueuesCreateInfo(const TArray<vk::QueueFamilyProperties>& QueueFamilyProperties);
    void ClearQueuesCreateInfo();

    void FillQueuePool(vk::Device* Device, const TArray<vk::QueueFamilyProperties>& QueueFamilyProperties);
    void ClearQueuePool();
    bool IsValid() override;

protected:
    TArray<FVulkanQueue*> GraphicsQueues;
    TArray<FVulkanQueue*> ComputeQueues;
    TArray<FVulkanQueue*> TransferQueues;
    TArray<FVulkanQueue*> SparseQueues;

    TArray<FVulkanQueue*> AllQueues;

protected:
    //Used only for Queue creation needs.
    TArray<std::vector<float>*> AllQueueFamilyPriorities;
};
