﻿#include "VulkanSwapChain.h"

#include "VulkanDevice.h"
#include "Render/RHI/Vulkan/Utils/VulkanUtils.h"
#include "Config/JsonConfig.h"

UVulkanSwapChain::UVulkanSwapChain(UVulkanDevice* InVulkanDevice, vk::SurfaceKHR* InSurface)
{
    ensureMsgf(InVulkanDevice->IsValid() && IsValid(InSurface) && JsonConfig->GetIsLoaded(), "Invalid PhysicalDevice or Surface, or JsonConfig isn't load!");

    VulkanDevice = InVulkanDevice;

    const FSwapChainSupportDetails SupportDetails = QuerySwapChainSupportDetails(VulkanDevice->GetPhysicalDevice(), InSurface);

    vk::SwapchainCreateInfoKHR SwapChainCreateInfo;
    SwapChainCreateInfo.minImageCount = ChooseImageCount(SupportDetails.Capabilities, SupportDetails.Capabilities.minImageCount + 1);
    SwapChainCreateInfo.presentMode = ChoosePresentMode(SupportDetails.PresentModes, JsonConfig->PresentationMode);
    SwapChainCreateInfo.imageExtent = ChooseSwapExtent(SupportDetails.Capabilities, JsonConfig->ScreenResolution);
    SwapChainCreateInfo.imageArrayLayers = 1;
    SwapChainCreateInfo.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;
    SwapChainCreateInfo.preTransform = SupportDetails.Capabilities.currentTransform;
    SwapChainCreateInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
    SwapChainCreateInfo.oldSwapchain = VK_NULL_HANDLE;
    SwapChainCreateInfo.clipped = VK_TRUE;
    SwapChainCreateInfo.imageSharingMode = vk::SharingMode::eExclusive;

    const vk::SurfaceFormatKHR SurfaceFormat = ChooseSwapSurfaceFormat(SupportDetails.Formats, vk::Format::eR8G8B8Srgb, vk::ColorSpaceKHR::eSrgbNonlinear);
    SwapChainCreateInfo.imageFormat = SurfaceFormat.format;
    SwapChainCreateInfo.imageColorSpace = SurfaceFormat.colorSpace;
    SwapChainCreateInfo.surface = *InSurface;

    SwapChain = VulkanDevice->GetLogicalDevice()->createSwapchainKHR(SwapChainCreateInfo);
}

UVulkanSwapChain::~UVulkanSwapChain()
{
    ensureMsgf(VulkanDevice->IsValid(), "Can't destroy properly. Device Invalid!");
    SwapChainInfo = { };
    VulkanDevice->GetLogicalDevice()->destroySwapchainKHR(SwapChain);
    SwapChain = nullptr;
    VulkanDevice = nullptr;
    PRINT_FUNC();
}

FSwapChainSupportDetails UVulkanSwapChain::QuerySwapChainSupportDetails(vk::PhysicalDevice* InPhysicalDevice, vk::SurfaceKHR* InSurface)
{
    FSwapChainSupportDetails SupportDetails;
    SupportDetails.Capabilities = InPhysicalDevice->getSurfaceCapabilitiesKHR(*InSurface);
    SupportDetails.PresentModes = InPhysicalDevice->getSurfacePresentModesKHR(*InSurface);
    SupportDetails.Formats = InPhysicalDevice->getSurfaceFormatsKHR(*InSurface);

    return SupportDetails;
}

vk::SurfaceFormatKHR UVulkanSwapChain::ChooseSwapSurfaceFormat(const TArray<vk::SurfaceFormatKHR>& InAvailableFormats,
                                                               const vk::Format& InFormat,
                                                               const vk::ColorSpaceKHR& InColorSpace)
{
    for (const auto& AvailableFormat : InAvailableFormats)
    {
        if (AvailableFormat.format == InFormat && AvailableFormat.colorSpace == InColorSpace)
            return AvailableFormat;
    }

    return InAvailableFormats[0];
}

vk::PresentModeKHR UVulkanSwapChain::ChoosePresentMode(const TArray<vk::PresentModeKHR>& InAvailablePresentModes, EGraphicsPresentationMode& InPresentMode)
{
    vk::PresentModeKHR RequestedPresentMode = VulkanUtils::ToPresentModeKHR(InPresentMode);

    const bool IsSupported = TArrayUtils::Contains(InAvailablePresentModes, RequestedPresentMode);

    if (!IsSupported)
    {
        LOG("Requested present mode isn't supported by GPU!");
        InPresentMode = EGraphicsPresentationMode::None;
        RequestedPresentMode = VulkanUtils::ToPresentModeKHR(InPresentMode);
    }
    return RequestedPresentMode;
}

vk::Extent2D UVulkanSwapChain::ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR& InCapabilities, FVector2D& InResolution) const
{
    if (InCapabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
        return InCapabilities.currentExtent;

    const VkExtent2D ActualExtent = {
        std::clamp(uint32(InResolution.X), InCapabilities.minImageExtent.width, InCapabilities.maxImageExtent.width),
        std::clamp(uint32(InResolution.Y), InCapabilities.minImageExtent.height, InCapabilities.maxImageExtent.height)
    };
    InResolution = FVector2D(float(ActualExtent.width), float(ActualExtent.height));
    return ActualExtent;
}

uint32 UVulkanSwapChain::ChooseImageCount(const vk::SurfaceCapabilitiesKHR& InCapabilities, const uint32 InCount) const
{
    return std::clamp(InCount, InCapabilities.minImageCount, InCapabilities.maxImageCount);
}
