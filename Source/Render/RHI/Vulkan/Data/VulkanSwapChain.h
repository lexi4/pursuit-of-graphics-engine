﻿#pragma once

#include <Core/CoreTypes.h>

#include "Core/BaseClass.h"
#include "Core/BaseStructure.h"

class UVulkanDevice;

struct FVulkanSwapChainInfo : FBaseStructure
{
public:
    vk::PresentModeKHR PresentMode = vk::PresentModeKHR::eImmediate;
    vk::SurfaceCapabilitiesKHR SurfaceCapabilities;
    vk::Extent2D Extent;
    vk::Format Format = vk::Format::eUndefined;
};

struct FSwapChainSupportDetails : FBaseStructure
{
    vk::SurfaceCapabilitiesKHR Capabilities;
    TArray<vk::SurfaceFormatKHR> Formats;
    TArray<vk::PresentModeKHR> PresentModes;
};

class UVulkanSwapChain : public UBaseClass
{
public:
    UVulkanSwapChain() = delete;

    UVulkanSwapChain(UVulkanDevice* InVulkanDevice, vk::SurfaceKHR* InSurface);
    ~UVulkanSwapChain() override;

    const vk::SwapchainKHR* GetSwapChain() const { return &SwapChain; }

protected:
    UVulkanDevice* VulkanDevice = nullptr;
    vk::SwapchainKHR SwapChain = nullptr;
    FVulkanSwapChainInfo SwapChainInfo;

protected:
    FSwapChainSupportDetails QuerySwapChainSupportDetails(vk::PhysicalDevice* InPhysicalDevice, vk::SurfaceKHR* InSurface);

protected:
    vk::SurfaceFormatKHR ChooseSwapSurfaceFormat(const TArray<vk::SurfaceFormatKHR>& InAvailableFormats, const vk::Format& InFormat, const vk::ColorSpaceKHR& InColorSpace);
    vk::PresentModeKHR ChoosePresentMode(const TArray<vk::PresentModeKHR>& InAvailablePresentModes, EGraphicsPresentationMode& InPresentMode);
    vk::Extent2D ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR& InCapabilities, FVector2D& InResolution) const;
    uint32 ChooseImageCount(const vk::SurfaceCapabilitiesKHR& InCapabilities, const uint32 InCount) const;
};