﻿#include "VulkanDebug.h"

UVulkanDebug::UVulkanDebug(vk::Instance* InVulkanInstance)
{
    if(!IsValid(InVulkanInstance))
    {
        return;
    }
    VulkanInstance = InVulkanInstance;

    const vk::DebugUtilsMessageSeverityFlagsEXT severityFlags = {
        vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
        vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo |
        vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
        vk::DebugUtilsMessageSeverityFlagBitsEXT::eError
    };
    const vk::DebugUtilsMessageTypeFlagsEXT messageTypeFlags = {
        vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
        vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
        vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation
    };

    if constexpr (!CONST_RENDER::VULKAN::IS_ENABLED_VALIDATION_LAYERS)
    {
        return;
    }
    PRINT_FUNC();

    pfnVkCreateDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>( VulkanInstance->getProcAddr( "vkCreateDebugUtilsMessengerEXT" ) );
    if ( !pfnVkCreateDebugUtilsMessengerEXT )
    {
        std::cerr << "GetInstanceProcAddr: Unable to find pfnVkCreateDebugUtilsMessengerEXT function." << std::endl;
        exit( 1 );
    }

    pfnVkDestroyDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(
      VulkanInstance->getProcAddr( "vkDestroyDebugUtilsMessengerEXT" ) );
    if ( !pfnVkDestroyDebugUtilsMessengerEXT )
    {
        std::cerr << "GetInstanceProcAddr: Unable to find pfnVkDestroyDebugUtilsMessengerEXT function." << std::endl;
        exit( 1 );
    }

    const vk::DebugUtilsMessengerCreateInfoEXT DebugUtilsMessengerCreateInfo = { {}, severityFlags, messageTypeFlags, &vkDebugMessagePrinter };
    DebugUtilsMessenger = VulkanInstance->createDebugUtilsMessengerEXT(DebugUtilsMessengerCreateInfo);
}

UVulkanDebug::~UVulkanDebug()
{
    ensure(VulkanInstance);
    VulkanInstance->destroyDebugUtilsMessengerEXT(DebugUtilsMessenger);
    pfnVkCreateDebugUtilsMessengerEXT = nullptr;
    pfnVkDestroyDebugUtilsMessengerEXT = nullptr;
    DebugUtilsMessenger = nullptr;
    VulkanInstance = nullptr;
}
