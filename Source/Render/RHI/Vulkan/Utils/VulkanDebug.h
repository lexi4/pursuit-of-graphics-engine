﻿#pragma once
#include "Render/RHI/Vulkan/VulkanIncludes.h"

class UVulkanDebug
{
public:
    UVulkanDebug(vk::Instance* InVulkanInstance);
    ~UVulkanDebug();

    inline static PFN_vkCreateDebugUtilsMessengerEXT  pfnVkCreateDebugUtilsMessengerEXT;
    inline static PFN_vkDestroyDebugUtilsMessengerEXT pfnVkDestroyDebugUtilsMessengerEXT;

    vk::DebugUtilsMessengerEXT DebugUtilsMessenger;

protected:
    vk::Instance* VulkanInstance;
};

VKAPI_ATTR inline VkResult VKAPI_CALL vkCreateDebugUtilsMessengerEXT( VkInstance                                 instance,
                                                                      const VkDebugUtilsMessengerCreateInfoEXT * pCreateInfo,
                                                                      const VkAllocationCallbacks *              pAllocator,
                                                                      VkDebugUtilsMessengerEXT *                 pMessenger )
{
    LOG();
    return UVulkanDebug::pfnVkCreateDebugUtilsMessengerEXT( instance, pCreateInfo, pAllocator, pMessenger );
}

VKAPI_ATTR inline void VKAPI_CALL vkDestroyDebugUtilsMessengerEXT( VkInstance                    instance,
                                                                   VkDebugUtilsMessengerEXT      messenger,
                                                                   VkAllocationCallbacks const * pAllocator )
{
    LOG();
    return UVulkanDebug::pfnVkDestroyDebugUtilsMessengerEXT( instance, messenger, pAllocator );
}

VKAPI_ATTR inline VkBool32 VKAPI_CALL vkDebugMessagePrinter( VkDebugUtilsMessageSeverityFlagBitsEXT       messageSeverity,
                                                             VkDebugUtilsMessageTypeFlagsEXT              messageTypes,
                                                             VkDebugUtilsMessengerCallbackDataEXT const * pCallbackData,
                                                             void * /*pUserData*/ )
{
    FString message;

    message += to_string( static_cast<vk::DebugUtilsMessageSeverityFlagBitsEXT>( messageSeverity ) ) + ": " +
               to_string( static_cast<vk::DebugUtilsMessageTypeFlagsEXT>( messageTypes ) ) + ":\n";
    message += FString( "\t" ) + "messageIDName   = <" + pCallbackData->pMessageIdName + ">\n";
    message += FString( "\t" ) + "messageIdNumber = " + std::to_string( pCallbackData->messageIdNumber ) + "\n";
    message += FString( "\t" ) + "message         = <" + pCallbackData->pMessage + ">\n";
    if ( 0 < pCallbackData->queueLabelCount )
    {
        message += FString( "\t" ) + "Queue Labels:\n";
        for ( uint8_t i = 0; i < pCallbackData->queueLabelCount; i++ )
        {
            message += FString( "\t\t" ) + "labelName = <" + pCallbackData->pQueueLabels[i].pLabelName + ">\n";
        }
    }
    if ( 0 < pCallbackData->cmdBufLabelCount )
    {
        message += FString( "\t" ) + "CommandBuffer Labels:\n";
        for ( uint8_t i = 0; i < pCallbackData->cmdBufLabelCount; i++ )
        {
            message += FString( "\t\t" ) + "labelName = <" + pCallbackData->pCmdBufLabels[i].pLabelName + ">\n";
        }
    }
    if ( 0 < pCallbackData->objectCount )
    {
        for ( uint8_t i = 0; i < pCallbackData->objectCount; i++ )
        {
            message += FString( "\t" ) + "Object " + std::to_string( i ) + "\n";
            message += FString( "\t\t" ) + "objectType   = " +
                       vk::to_string( static_cast<vk::ObjectType>( pCallbackData->pObjects[i].objectType ) ) + "\n";
            message +=
              FString( "\t\t" ) + "objectHandle = " + std::to_string( pCallbackData->pObjects[i].objectHandle ) + "\n";
            if ( pCallbackData->pObjects[i].pObjectName )
            {
                message += FString( "\t\t" ) + "objectName   = <" + pCallbackData->pObjects[i].pObjectName + ">\n";
            }
        }
    }
    LOG(message);
    return false;
}