﻿#pragma once
#include "Core/CoreTypes.h"

#include <vulkan/vulkan_core.h>

namespace VulkanUtils
{
    inline void Check(const VkResult& InResult)
    {
        if(InResult != VK_SUCCESS)
        {
            throw std::runtime_error("failed to create window surface!");
        }
    }

    inline vk::PresentModeKHR ToPresentModeKHR(const EGraphicsPresentationMode InGraphicsPresentationMode)
    {
        switch(InGraphicsPresentationMode)
        {
        case EGraphicsPresentationMode::VSync:
            return vk::PresentModeKHR::eFifo;
        case EGraphicsPresentationMode::Adaptive:
            return vk::PresentModeKHR::eFifoRelaxed;
        case EGraphicsPresentationMode::TripleBuffer:
            return vk::PresentModeKHR::eMailbox;
        default:
            return vk::PresentModeKHR::eImmediate;
        }
    }

    inline EGraphicsPresentationMode FromPresentModeKHR(const vk::PresentModeKHR InPresentModeKHR)
    {
        switch(InPresentModeKHR)
        {
        case vk::PresentModeKHR::eFifo:
            return EGraphicsPresentationMode::VSync;
        case vk::PresentModeKHR::eFifoRelaxed:
            return EGraphicsPresentationMode::Adaptive;
        case vk::PresentModeKHR::eMailbox:
            return EGraphicsPresentationMode::TripleBuffer;
        default:
            return EGraphicsPresentationMode::None;
        }
    }
}