#include "VulkanRHI.h"
#include "VulkanIncludes.h"
#include "Data/VulkanDevice.h"
#include "Render/Window/Window.h"
#include "Data/VulkanSwapChain.h"
#include "Utils/VulkanDebug.h"

namespace AppInfo
{
    static FString AppName = CONST_RENDER::WINDOW::NAME;
    static uint32 AppVersion = VK_MAKE_VERSION(0, 0, 1);
    static FString EngineName = "Pursuit";
    static uint32 EngineVersion = VK_MAKE_VERSION(0, 0, 1);
    static uint32 VulkanVersion = VK_API_VERSION_1_2;
}

void UVulkanRHI::InitInstance(const TArray<FString>& InInstanceExtensions, const TArray<FString>& InValidationLayers)
{
    PRINT_FUNC();
    const vk::ApplicationInfo ApplicationInfo = {
        AppInfo::AppName.c_str(),
        AppInfo::AppVersion,
        AppInfo::EngineName.c_str(),
        AppInfo::EngineVersion,
        AppInfo::VulkanVersion
    };

    std::vector<char const*> RequestedExtensions;
    std::vector<char const*> RequestedValidationLayers;
    StringsToCharsVector(InInstanceExtensions, RequestedExtensions);
    StringsToCharsVector(InValidationLayers, RequestedValidationLayers);

    const vk::InstanceCreateInfo InstanceCreateInfo = {
        vk::InstanceCreateFlags(),
        &ApplicationInfo,
        RequestedValidationLayers,
        RequestedExtensions
    };

    Instance = createInstance(InstanceCreateInfo);
}

void UVulkanRHI::InitValidationLayersPrinter(vk::Instance& InInstance)
{
    ValidationLayersPrinter = new UVulkanDebug(&InInstance);
}

void UVulkanRHI::InitDevices(const TArray<FString>& InDeviceExtensions, const TArray<FString>& InLayers)
{
    PRINT_FUNC();
    const TArray<vk::PhysicalDevice> PhysicalDevices = Instance.enumeratePhysicalDevices();
    
    LOG("== Devices BEGIN ==");
    for (const vk::PhysicalDevice& PhysicalDevice : PhysicalDevices)
    {
        LOG(PhysicalDevice.getProperties().deviceName.data());
        VulkanDevices.Emplace(new UVulkanDevice(PhysicalDevice, InDeviceExtensions, InLayers));
    }
    LOG("== Devices END ==");
}

void UVulkanRHI::InitSwapChain()
{
    PRINT_FUNC();
    //ensureMsgf(!VulkanDevices.IsEmpty(), "Empty Devices");
    ensure(VulkanDevices.IsEmpty());
    Swapchain = new UVulkanSwapChain(VulkanDevices[0], &Surface);
}

void UVulkanRHI::DeinitSwapChain()
{
    delete Swapchain;
    Swapchain = nullptr;
}

void UVulkanRHI::DeinitDevices()
{
    for(const auto* Device : VulkanDevices)
    {
        delete Device;
    }
    VulkanDevices.Empty();
}

void UVulkanRHI::DeinitValidationLayersPrinter()
{
}

void UVulkanRHI::DeinitSurface()
{
}

void UVulkanRHI::DeinitInstance()
{
}

void UVulkanRHI::InitSurface(UWindow* InWindow, const vk::Instance& InInstance, const vk::SurfaceKHR& InSurface)
{
    InWindow->InitVulkanSurface(Instance, Surface);
}

UVulkanRHI::UVulkanRHI(UWindow* InWindow)
{
    PRINT_FUNC();

    if (!IsValid(InWindow))
    {
        throw std::invalid_argument(FString(__FUNCTION__) + ": Invalid argument <UWindow* InWindow>");
    }

    auto& LayersAndExtensions = CONST_RENDER::VULKAN::LAYERS_AND_EXTENSIONS;
    LayersAndExtensions.Init();
    const TArray<FString>& DeviceExtensions = LayersAndExtensions.GetDeviceExtensions();
    const TArray<FString>& InstanceExtensions = LayersAndExtensions.GetInstanceExtensions();
    const TArray<FString>& Layers = LayersAndExtensions.GetLayers();

    LOG(Layers.ToString());

    InitInstance(InstanceExtensions, Layers);

    if(CONST_RENDER::VULKAN::IS_ENABLED_VALIDATION_LAYERS)
        InitValidationLayersPrinter(Instance);

    InitSurface(InWindow, Instance, Surface);
    InitDevices(DeviceExtensions, Layers);
    InitSwapChain();
}

UVulkanRHI::~UVulkanRHI()
{
    PRINT_FUNC();
    DeinitSwapChain();
    DeinitDevices();
    DeinitSurface();

    if(CONST_RENDER::VULKAN::IS_ENABLED_VALIDATION_LAYERS)
        DeinitValidationLayersPrinter();

    DeinitInstance();
}

void UVulkanRHI::RenderFrame()
{
    PRINT_FUNC();
    return;
}
