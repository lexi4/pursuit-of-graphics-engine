#pragma once

#include <Core/CoreTypes.h>

#include "Render/RHI/RenderInterface.h"

class UVulkanSwapChain;
class UVulkanDebug;
class UVulkanDevice;
struct FVulkanDevice;

class UVulkanRHI : public IRender
{
public:
    UVulkanRHI() = delete;
    UVulkanRHI(UWindow* InWindow);
    void RenderFrame() override;
    ~UVulkanRHI() override;

private:
    void InitInstance(const TArray<FString>& InInstanceExtensions, const TArray<FString>& InValidationLayers);
    void InitValidationLayersPrinter(vk::Instance& InInstance);
    void InitSurface(UWindow* InWindow, const vk::Instance& InInstance, const vk::SurfaceKHR& InSurface);
    void InitDevices(const TArray<FString>& InDeviceExtensions, const TArray<FString>& InLayers);
    void InitSwapChain();

    void DeinitSwapChain();
    void DeinitDevices();
    void DeinitValidationLayersPrinter();
    void DeinitSurface();
    void DeinitInstance();

private:
    vk::SurfaceKHR         Surface = nullptr;
    vk::Instance           Instance = nullptr;
    TArray<UVulkanDevice*> VulkanDevices = TArray<UVulkanDevice*>();
    UVulkanDevice*         MainVulkanDevice = nullptr;
    UVulkanSwapChain*      Swapchain = nullptr;
    UVulkanDebug*          ValidationLayersPrinter = nullptr;
};
