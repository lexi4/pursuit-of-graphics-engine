﻿#include "Render.h"

#include "Config/Constants/RenderConstants.h"
#include "RHI/Vulkan/VulkanRHI.h"
#include "Data/ERenderAPI.h"
#include "Window/Window.h"
#include "Render/RHI/RenderInterface.h"

URender::URender()
{
    PRINT_FUNC();
    Window = new UWindow();

    switch (CONST_RENDER::RENDER_API)
    {
        case ERenderAPI::Vulkan:
            {
                UVulkanRHI* Vulkan = new UVulkanRHI(Window);
                RenderHardwareInterface = StaticCast<IRender>(Vulkan);
            }
        break;
        default:
            std::cerr << "[URender::URender] Invalid RENDER_API value!" << std::endl;
    }
}

void URender::RenderFrame() const
{
    PRINT_FUNC();
    RenderHardwareInterface->RenderFrame();
}

URender::~URender()
{
    PRINT_FUNC();
    delete RenderHardwareInterface;
    delete Window;
}
