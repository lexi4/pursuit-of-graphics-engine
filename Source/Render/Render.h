﻿#pragma once

class IRender;
class UWindow;

class URender
{
public:
    URender();
    void RenderFrame() const;
    ~URender();

    IRender* RenderHardwareInterface = nullptr;
    UWindow* Window = nullptr;
};
