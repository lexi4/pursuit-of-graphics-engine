﻿#include "Window.h"

#include <GLFW/glfw3.h>

#include "Config/JsonConfig.h"
#include "Config/Constants/RenderConstants.h"

UWindow::UWindow()
{
    LOG();
    const auto Result = glfwInit();
    if(Result != GLFW_TRUE)
    {
        throw std::runtime_error("failed to init GLFW!");
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    const FVector2D& WindowSize = JsonConfig->ScreenResolution;
    const FString& WindowName = CONST_RENDER::WINDOW::NAME;
    GlfwWindow = glfwCreateWindow((int)WindowSize.X, (int)WindowSize.Y, WindowName.c_str(), nullptr, nullptr);
}

UWindow::~UWindow()
{
    LOG();
    glfwDestroyWindow(GlfwWindow);
    glfwTerminate();
    GlfwWindow = nullptr;
}

void UWindow::InitVulkanSurface(vk::Instance& InVulkanInstance, vk::SurfaceKHR& InVulkanSurface)
{
    PRINT_FUNC();
    VkSurfaceKHR NewSurface_CType;
    const VkResult Result = glfwCreateWindowSurface(static_cast<VkInstance>(InVulkanInstance), GlfwWindow, nullptr, &NewSurface_CType);
    if(Result != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create window surface!");
    }
    InVulkanSurface = vk::SurfaceKHR(NewSurface_CType);
}

TArray<FString>* UWindow::GetWindowVulkanExtensions()
{
    uint32 ExtensionCount = 0;
    const auto Result = CharsToStrings(glfwGetRequiredInstanceExtensions(&ExtensionCount));
    
    FString Msg = "\n    GLFW Required Extensions:";
    for(auto GlfwExtension : *Result)
    {
        Msg+= "\n        " + GlfwExtension;
    }
    LOG(Msg);

    return Result;
}
