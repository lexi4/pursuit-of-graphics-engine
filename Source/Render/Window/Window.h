﻿#pragma once
#include "Core/CoreTypes.h"

struct GLFWwindow;

class UWindow
{
public:
    UWindow();
    ~UWindow();
    
    void InitVulkanSurface(vk::Instance& InVulkanInstance, vk::SurfaceKHR& InVulkanSurface);
    static TArray<FString>* GetWindowVulkanExtensions();

public:
    GLFWwindow* GlfwWindow = nullptr;
};
