﻿#pragma once 

#include <Render/Render.h>
#include <Render/Window/Window.h>
#include <CodeSandbox/UCodeSandbox.h>
#include <GLFW/glfw3.h>
#include <Core/Time.h>
#include "Config/JsonConfig.h"

int main() {
    
    LOG();
    JsonConfig->Load();

    const URender* RenderInstance = new URender();

    //Input events
    while (!glfwWindowShouldClose(RenderInstance->Window->GlfwWindow)) {
        glfwPollEvents();
    }
    delete RenderInstance;
    return 0;
}

